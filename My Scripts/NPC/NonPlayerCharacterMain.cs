﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NonPlayerCharacterMain : MonoBehaviour 
{
	public string m_strName;
	Transform m_goPlayer;
	
	NonPlayerCharacterConversationScript m_npcChat;
	
	void Start()
	{
		m_npcChat = GetComponent<NonPlayerCharacterConversationScript>();
	}
	
	void Update()
	{
		
	}
	
	public void On_ActionButton_Pressed(NetworkPlayer player)
	{
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		m_goPlayer = netManScript.GetPlayerObjectFromNetworkPlayer(player).transform;
		if(Vector3.Distance(transform.position, m_goPlayer.position)<=m_npcChat.getTalkRange())
		{
			m_npcChat.Converse();
		}
	}
	
	public string GetName()
	{
		return m_strName;
	}
	
	public Transform getPlayerTarget()
	{
		return m_goPlayer;
	}
	
	public void resetTarget()
	{
		m_goPlayer = null;
	}
}
