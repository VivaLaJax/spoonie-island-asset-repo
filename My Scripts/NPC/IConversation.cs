﻿using System.Collections;
using System.Collections.Generic;

public class IConversation 
{
	protected int m_nType = (int)ConversationTypeDefs.CONVO_TYPE_UNKNOWN;
	protected string m_strTitle;
	protected List<string> m_strArrDialog;
	protected List<string> m_strArrChoiceLinks;
	protected List<string> m_strArrChoiceText;
	
	public IConversation()
	{
		setType((int)ConversationTypeDefs.CONVO_TYPE_UNKNOWN);
		setTitle("");
		setDialog(new List<string>());
		setChoices(new List<string>());
		setLinks(new List<string>());
	}
	
	public IConversation(int nType, string strTitle, List<string> strArrLines, List<string> strArrChoices,
		List<string> strArrLinks)
	{
		setType(nType);
		setTitle(strTitle);
		setDialog(strArrLines);
		setChoices(strArrChoices);
		setLinks(strArrLinks);
	}
	
	public void setType(int nType)
	{
		m_nType = nType;
	}
	public int getType()
	{
		return m_nType;
	}
	
	public void setTitle(string strTitle)
	{
		m_strTitle = strTitle;
	}
	public string getTitle()
	{
		return m_strTitle;
	}
	
	public void setDialog(List<string> strArrLines)
	{
		m_strArrDialog = strArrLines;
	}
	public void addDialog(string strDialog)
	{
		m_strArrDialog.Add(strDialog);
	}
	public List<string> getDialogLines()
	{
		return m_strArrDialog;
	}
	
	public void setLinks(List<string> strArrLines)
	{
		m_strArrChoiceLinks = strArrLines;
	}
	public void addLink(string strLink)
	{
		m_strArrChoiceLinks.Add(strLink);
	}
	public List<string> getLinks()
	{
		return m_strArrChoiceLinks;
	}
	
	public void setChoices(List<string> strArrLines)
	{
		m_strArrChoiceText = strArrLines;
	}
	public void addChoice(string strChoice)
	{
		m_strArrChoiceText.Add(strChoice);
	}
	public List<string> getChoices()
	{
		return m_strArrChoiceText;
	}
	
	public string getDialogByIndex(int index)
	{
		return m_strArrDialog[index];
	}
	
	public string getChoiceByIndex(int index)
	{
		return m_strArrChoiceText[index];
	}
	
	public string getLinkByIndex(int index)
	{
		return m_strArrChoiceLinks[index];
	}
}
