using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NonPlayerCharacterConversationScript : MonoBehaviour 
{
	List<IConversation> m_ConversationTexts;
	IConversation m_currentConvo;
	int m_nConersationIndex;
	int m_nDialogIndex;
	bool m_bConvoBoxOpen;
	float m_nTalkRange = 5.0f;
	bool m_bChoiceInitiated = false;
	string m_strChoiceLink = "";
	
	NonPlayerCharacterMain m_npcMain;
	NPCGUIHandler m_npcGUI;
	
	const int CONVERSATION_OUT_OF_INDEX = 9999;
	public bool m_bLoggingEnabled = false;
	
	void Start () 
	{
		m_npcMain = GetComponent<NonPlayerCharacterMain>();
		m_npcGUI = GetComponent<NPCGUIHandler>();
		
		if(m_npcGUI!=null)
		{
			m_npcGUI.setNPCConvoScript(this);
			m_npcGUI.setNPCName(m_npcMain.GetName());
		}
		
		reset();
	}
	
	void Update () 
	{
		if(m_bConvoBoxOpen)
		{
			if(!checkPlayerInRange())
			{
				reset();
			}
		}
	}
		
	public void Converse()
	{
		if(m_currentConvo==null)
		{
			m_currentConvo = m_ConversationTexts[m_nConersationIndex];
		}
		
		bool bNextConvo = false;
		
		if(m_bConvoBoxOpen)
		{
			if(m_currentConvo.getType()==(int)ConversationTypeDefs.CONVO_TYPE_DIALOGUE)
			{
				//DisableSpeechBubbles();
				m_nDialogIndex++;
				if(!(m_nDialogIndex>=m_currentConvo.getDialogLines().Count))
				{
					EnableSpeechBubbleWithText(m_currentConvo.getDialogByIndex(m_nDialogIndex));
				}
				else
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("Dialog complete. Getting link");
					}
					//get next convo index from link here
					m_nConersationIndex = getConversationIndexFromLink(m_currentConvo.getLinkByIndex(0));
					bNextConvo = true;
				}
			}
			else if(m_currentConvo.getType()==(int)ConversationTypeDefs.CONVO_TYPE_CHOICE)
			{
				if(!m_bChoiceInitiated)
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("Choice initiated");
					}
					//DisableSpeechBubbles();
					EnableChoiceBubbleWithChoice(m_currentConvo);
					m_bChoiceInitiated = true;
					m_strChoiceLink = "";
				}
				else if(!m_strChoiceLink.Equals(""))
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("Choice has been made. Getting index");
					}
					m_nConersationIndex = getConversationIndexFromLink(m_strChoiceLink);
					bNextConvo = true;
				}
			}
			
			if(bNextConvo)
			{
				if(m_nConersationIndex>=m_ConversationTexts.Count)
				{
					reset();
				}
				else
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("New conversation. Setting convo variables to defaults");
					}
					DisableSpeechBubbles();
					m_currentConvo = m_ConversationTexts[m_nConersationIndex];
					m_nDialogIndex = 0;
					m_bChoiceInitiated = false;
					m_strChoiceLink = "";
					Converse();
				}
			}
		}
		else
		{
			if(m_currentConvo.getType()==(int)ConversationTypeDefs.CONVO_TYPE_DIALOGUE)
			{
				EnableSpeechBubbleWithText(m_currentConvo.getDialogByIndex(m_nDialogIndex));	
			}
			else if(m_currentConvo.getType()==(int)ConversationTypeDefs.CONVO_TYPE_CHOICE)
			{
				EnableChoiceBubbleWithChoice(m_currentConvo);
				m_bChoiceInitiated = true;
				m_strChoiceLink = "";
			}
			m_bConvoBoxOpen = true;
		}
	}
	
	void EnableSpeechBubbleWithText(string toSend)
	{
		if(Network.isClient)
		{			
			m_npcGUI.setUpDialogBox(toSend);
		}
	}
	
	void EnableChoiceBubbleWithChoice(IConversation convoChoice)
	{
		if(Network.isClient)
		{			
			m_npcGUI.setUpChoiceBox(convoChoice);
		}
	}
	
	void DisableSpeechBubbles()
	{
		m_npcGUI.disableBox();
		m_bConvoBoxOpen = false;
	}
	
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(transform.position,getTalkRange());
	}
	
	public void loadConversation(List<IConversation> convoArrVariables)
	{
		m_ConversationTexts = convoArrVariables;
	}
	
	bool checkPlayerInRange()
	{
		Transform goPlayer = m_npcMain.getPlayerTarget();
		if(goPlayer!=null)
		{
			if(Vector3.Distance(transform.position, goPlayer.position)<=m_nTalkRange)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public bool getConvoBoxOpen()
	{
		return m_bConvoBoxOpen;
	}
	
	public float getTalkRange()
	{
		return m_nTalkRange;
	}
	
	int getConversationIndexFromLink(string strLink)
	{
		for(int i=0; i<m_ConversationTexts.Count; ++i)
		{
			if(m_bLoggingEnabled)
			{
				Debug.Log("Title is equal: " + strLink + ":" + (m_ConversationTexts[i].getTitle()));
			}
			if(strLink.Equals(m_ConversationTexts[i].getTitle()))
			{
				if(m_bLoggingEnabled)
				{
					Debug.Log("Index: " + i + " from link: " + strLink);
				}
				return i;
			}
		}
		
		if(m_bLoggingEnabled)
		{
			Debug.Log("Conversation link index could not be found for title: " + strLink);
		}
		return CONVERSATION_OUT_OF_INDEX;
	}
	
	public void setChoiceLink(string strLink)
	{
		m_strChoiceLink = strLink;
	}
	
	void reset()
	{
		if(m_bLoggingEnabled)
		{
			Debug.Log("Conversation reset");
		}
		DisableSpeechBubbles();
		m_bConvoBoxOpen = false;
		m_nConersationIndex = 0;
		m_nDialogIndex = 0;
		m_npcMain.resetTarget();
		m_currentConvo = null;
		m_bChoiceInitiated = false;
		m_strChoiceLink = "";
	}
}
