﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCGUIHandler : MonoBehaviour 
{
	private string m_strNPCName;
	private string m_strDialog;
	private int m_nButtonNumbers;
	NonPlayerCharacterConversationScript m_npcConvoScript;
	List<string> m_lstStrChoiceLabel;
	List<string> m_lstStrChoiceLink;
	
	bool m_bBoxOpen;
	bool m_bCurrentConvoIsChoice;
	
	public bool m_bLoggingEnabled = false;
	
	void Start()
	{
		m_bBoxOpen = false;
		m_bCurrentConvoIsChoice = false;
		m_strDialog = "";
		m_lstStrChoiceLabel = new List<string>();
		m_lstStrChoiceLink = new List<string>();
		m_nButtonNumbers = 0;
	}
	
	void Update()
	{
		
	}
	
	void OnGUI()
	{
		if(m_bBoxOpen)
		{
			float nBoxHeight = Screen.height/4.0f;
			float nBoxWidth = Screen.width;
			float nBoxX = 0;
			float nBoxY = Screen.height - nBoxHeight;
			float nBuffer = 2.0f;
			
			float nNameHeight = nBoxHeight/5;
			float nNameWidth = nBoxWidth/4;
			float nNameX = nBoxX+nBuffer;
			float nNameY = nBoxY+nBuffer;
			
			//display box
			GUI.Box(new Rect(nBoxX,nBoxY,nBoxWidth,nBoxHeight),"");
			//display name
			GUI.Label(new Rect(nNameX,nNameY,nNameWidth,nNameHeight), m_strNPCName);
			//display dialog
			GUI.Label(new Rect(nNameX,nNameY+nBuffer+nNameHeight, nBoxWidth-(2*nBuffer), nBoxHeight/2), m_strDialog);
			
			//display choices
			if(m_bCurrentConvoIsChoice)
			{
				float nButtonWdith = (nBoxWidth-2*nBuffer)/(float)m_nButtonNumbers;
				float nButtonHeight = nBoxHeight/4;
				float nButtonX = nBuffer;
				float nButtonY = Screen.height - nButtonHeight - nBuffer;
				
				for(int i=0; i<m_nButtonNumbers; ++i)
				{
					if(GUI.Button(new Rect(nButtonX+(i*nButtonWdith),nButtonY,nButtonWdith,nButtonHeight),m_lstStrChoiceLabel[i]))
					{
						if(m_npcConvoScript!=null)
						{
							m_npcConvoScript.setChoiceLink(m_lstStrChoiceLink[i]);
							if(m_bLoggingEnabled)
							{
								Debug.Log("Choice link set to: " + m_lstStrChoiceLink[i] + " and sent to convo script");
							}
							m_npcConvoScript.Converse();
						}
						else
						{
							if(m_bLoggingEnabled)
							{
								Debug.Log("NPCConvoScript is null");
							}
						}
					}
				}
			}
		}
	}
	
	string getNPCName()
	{
		return m_strNPCName;
	}
	
	string getDialogString()
	{
		return m_strDialog;
	}
	
	public void setNPCName(string strName)
	{
		m_strNPCName = strName;
		if(m_bLoggingEnabled)
		{
			Debug.Log("Set NPC Name: " + m_strNPCName);
		}
	}
	
	void setDialogString(string strDia)
	{
		m_strDialog = strDia;
	}
	
	public void setNPCConvoScript(NonPlayerCharacterConversationScript npcScript)
	{
		m_npcConvoScript = npcScript;
	}
	
	public void setUpDialogBox(string strDialog)
	{
		setDialogString(strDialog);
		if(m_bLoggingEnabled)
		{
			Debug.Log("Set Dialog string: " + strDialog);
		}
		//turn on box
		m_bBoxOpen = true;
		
	}
	
	public void setUpChoiceBox(IConversation convoChoice)
	{
		setUpDialogBox(convoChoice.getDialogByIndex(0));
		//turn on isChoice
		m_bCurrentConvoIsChoice = true;
		
		m_nButtonNumbers = convoChoice.getChoices().Count;
		if(m_bLoggingEnabled)
		{
			Debug.Log("Set Choice numbers: " + m_nButtonNumbers);
		}
		//set up list of buttons texts
		//set up list of button links
		for(int i=0; i<m_nButtonNumbers; ++i)
		{
			m_lstStrChoiceLabel.Add(convoChoice.getChoiceByIndex(i));
			m_lstStrChoiceLink.Add(convoChoice.getLinkByIndex(i));
		}
	}
	
	public void disableBox()
	{
		m_bBoxOpen = false;
		m_bCurrentConvoIsChoice = false;
		setDialogString("");
		m_lstStrChoiceLabel = new List<string>();
		m_lstStrChoiceLink = new List<string>();
		m_nButtonNumbers = 0;		
	}
}
