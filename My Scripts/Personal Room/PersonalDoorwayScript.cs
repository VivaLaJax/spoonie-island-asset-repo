using UnityEngine;
using System.Collections;

public class PersonalDoorwayScript : DoorWayScript 
{	
	void OnTriggerEnter(Collider other)
	{
		if(!Network.isServer)
		{
			if(!hasBeenActivated)
			{
				if(other.tag=="Player")
				{
					PlayerCharacterScript playScript = other.gameObject.GetComponent<PlayerCharacterScript>();
					if(playScript.GetOwner()==Network.player)
					{
						GameObject personalRoomManager = GameObject.FindGameObjectWithTag("PersonalRoomManager");
						PersonalRoomManagerScript roomManScript = personalRoomManager.GetComponent<PersonalRoomManagerScript>();
						GameObject goFloor = GameObject.FindGameObjectWithTag("Floor");
						roomManScript.leavePersonalRoom(Network.player,goFloor.GetComponent<LevelAssetScript>().getLevelName(),destination,source);
					
						hasBeenActivated = true;
					}
				}
			}
		}
	}
}
