using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class PersonalRoomFileHelper : ScriptableObject
{
	const string SAVE_FILE_PATH = "c:\\SpoonieRoomFile.txt";
	Queue qFileObjectQueue;
	
	public PersonalRoomFileHelper()
	{
		qFileObjectQueue = new Queue();
		qFileObjectQueue.Enqueue(null);
		loadRoomFromFile();
	}
	
	public bool savePersonalLevelAssets(string strLevelName)
	{
		string lines = "";
		
		StreamWriter file = new StreamWriter(SAVE_FILE_PATH);
		
		GameObject roomFloor = GameObject.FindGameObjectWithTag("Floor");
		Vector3 roomSpawnPos = roomFloor.transform.position;
		
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			LevelAssetScript assetScript = go.GetComponent<LevelAssetScript>();
			if(assetScript!=null)
			{
				string strGameObjectName = go.name.Remove(go.name.Length-7);
				if(assetScript.getLevelName()==strLevelName && strGameObjectName!="PersonalRoomFloor")
				{
					go.transform.position-=roomSpawnPos;
					
					//save to file
					lines+=strGameObjectName+ "|";
					lines+=go.transform.position.x.ToString() + ",";
					lines+=go.transform.position.y.ToString() + ",";
					lines+=go.transform.position.z.ToString() + "|";
					lines+=go.transform.rotation.x.ToString() + ",";
					lines+=go.transform.rotation.y.ToString() + ",";
					lines+=go.transform.rotation.z.ToString() + ",";
					lines+=go.transform.rotation.w.ToString() + "|";
					lines+="\n";
				}
			}
		}
		
		try
		{
			if(lines!="")
			{
				file.WriteLine(lines);
			}
		}
		catch(IOException e)
		{
			Debug.Log(e.ToString());
			file.Close();
			return false;
		}
		
		file.Close();
		return true;
	}
	
	public GameObjectFromFile getNextObject()
	{
		if(qFileObjectQueue.Count==0)
		{
			return null;
		}
		
		GameObjectFromFile fileObject = (GameObjectFromFile)qFileObjectQueue.Dequeue();
		
		return fileObject;
	}
	
	void loadRoomFromFile()
	{
		string[] fileText;
		
		if(File.Exists(SAVE_FILE_PATH))
		{
			fileText = File.ReadAllLines(SAVE_FILE_PATH);
			
			GameObjectFromFile fileObject;
			
			if(fileText.Length>0)
			{
				qFileObjectQueue.Dequeue();
				
				for(int i=0; i<fileText.Length-1; i++)
				{
					fileObject = new GameObjectFromFile();
					
					string[] fileObjectComponents = fileText[i].Split('|');
					
					fileObject.setObjectName(fileObjectComponents[0]);
					fileObject.setPosition(fileObjectComponents[1]);
					fileObject.setRotation(fileObjectComponents[2]);
					
					qFileObjectQueue.Enqueue(fileObject);
				}
			}
		}
	}
}
