using UnityEngine;
using System.Collections;

public class PersonalRoomManagerScript : MonoBehaviour 
{
	ArrayList loadedPersonalRooms;
	int lowestOpenSpot;
	NetworkManagerScript netManScript;
	
	public GameObject goRoomFloor; 
	public const string LEVEL_MASTER_NAME = "PersonalRoomOf";
	
	void Start () 
	{
		lowestOpenSpot = 0;
		loadedPersonalRooms = new ArrayList();
		netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		if(Network.isClient)
		{
			networkView.RPC ("RequestLowestOpenSpot",netManScript.getServerInfo(),Network.player);
		}
	}
	
	[RPC]
	public void createPersonalRoom(NetworkPlayer player, int spot)
	{		
		//find lowest index
		lowestOpenSpot = spot;
		
		//add level name+char index to loadedPersonalRooms(lowest index)
		string level = LEVEL_MASTER_NAME + player.ToString();
		
		if(Network.isServer)
		{
			loadedPersonalRooms.Insert(lowestOpenSpot,level);
		}
		else
		{
			loadedPersonalRooms.Add(level);
		}
		
		//set curr level to -99 + lowest index
		GameObject playerCharacter = netManScript.GetPlayerObjectFromNetworkPlayer(player);
		if(playerCharacter!=null)
		{
			playerCharacter.GetComponent<PlayerCharacterScript>().SetCurrentLevel(-99+lowestOpenSpot);
		}
		
		//create basic objects for the level here
		Vector3 spawnPos = createBasicRoomObjects(spot);
		
		if(!Network.isServer)
		{
			networkView.RPC ("createPersonalRoom",netManScript.getServerInfo(),player,lowestOpenSpot);
			Debug.Log("Client: About to load personal room: " + level + " (" + (-99+lowestOpenSpot) + ")");
			
			//create all personal objects and tell server
			createPersonalObjectsFromFile(spawnPos);
			
			
			//transport player!!!!!!!!!!!!!!!!!
			if(playerCharacter!=null)
			{
				playerCharacter.transform.position = spawnPos+(new Vector3(0,4,2));
			}
		}
		//handle networked objects here if needed
		else
		{					
			//networkView.RPC("PropagateID",player,manager.getViewId(),level);

			//Disable new ids!!!!!!!!!!!!!!!
			//disableNewIdsForLevel(level,player);
		}
	}
	
	[RPC]
	public void leavePersonalRoom(NetworkPlayer player, string strLevelName, string strDestination, string strSource)
	{		
		//remove level from list
		loadedPersonalRooms.Remove(strLevelName);
		//set char level to master
		int nMasterPersonalIndex = netManScript.getLevelIndex("MasterPersonalRoom");
		netManScript.GetPlayerObjectFromNetworkPlayer(player).GetComponent<PlayerCharacterScript>().SetCurrentLevel(nMasterPersonalIndex);
		
		if(!Network.isServer)
		{
			//save the room assets
			PersonalRoomFileHelper fileHelper = ScriptableObject.CreateInstance<PersonalRoomFileHelper>();
			fileHelper.savePersonalLevelAssets(strLevelName);
			
			//tell server
			networkView.RPC("leavePersonalRoom",netManScript.getServerInfo(),player,strLevelName,strDestination,strSource);
			
			//request level change from portal
			netManScript.RequestLevelFromPortal(strDestination,strSource,player);
		}
		
		//delete assets
		netManScript.deleteLevelAssets(strLevelName);		
	}
	
	[RPC]
	void ReceivePersonalObject(string strName, Vector3 vecPos, Quaternion quatRot, string strLevelName, Vector3 spawnPos)
	{
		GameObject goRoomObject = (GameObject)Instantiate(Resources.Load(strName),vecPos,quatRot);
		goRoomObject.GetComponent<LevelAssetScript>().attachedLevelName = strLevelName;
		goRoomObject.transform.position+=spawnPos;
	}
	
	int findLowestOpenRoomIndex()
	{
		int nRetVal = -99;
		
		if(loadedPersonalRooms==null)
		{
			loadedPersonalRooms = new ArrayList();
		}
		
		//add empty strings to arraylist if necessary to facilitate insert
		if(loadedPersonalRooms.Count==0)
		{
			loadedPersonalRooms.Add("");
			nRetVal = 0;
		}
		else
		{
			for(int i=0; i<loadedPersonalRooms.Count; i++)
			{
				if((string)loadedPersonalRooms[i]=="")
				{
					return i;
				}
			}
		}
		
		if(nRetVal==-99)
		{
			loadedPersonalRooms.Add("");
			nRetVal = loadedPersonalRooms.Count-1;
		}
		
		return nRetVal;
	}
	
	//return spawn position
	Vector3 createBasicRoomObjects(int spot)
	{
		Vector3 vecRetPos = new Vector3((lowestOpenSpot*(goRoomFloor.transform.localScale.x*2)),0,-1000);
		
		GameObject goFloor = (GameObject)Instantiate(goRoomFloor,vecRetPos,Quaternion.identity);
		if(Network.isServer)
		{
			goFloor.GetComponent<LevelAssetScript>().attachedLevelName = (string)loadedPersonalRooms[spot];
		}
		else
		{
			goFloor.GetComponent<LevelAssetScript>().attachedLevelName = (string)loadedPersonalRooms[0];
		}
		
		return vecRetPos;
	}
	
	//create all personal objects and tell server
	void createPersonalObjectsFromFile(Vector3 spawnPos)
	{		
		PersonalRoomFileHelper fileHelper = ScriptableObject.CreateInstance<PersonalRoomFileHelper>();
		GameObjectFromFile objectStructure = fileHelper.getNextObject();
		while(objectStructure!=null)
		{
			GameObject goRoomObject = (GameObject)Instantiate(Resources.Load(objectStructure.getObjectName()),objectStructure.getPosition(),
				objectStructure.getRotation());
			goRoomObject.GetComponent<LevelAssetScript>().attachedLevelName = (string)loadedPersonalRooms[lowestOpenSpot];
			goRoomObject.transform.position+=spawnPos;
			networkView.RPC("ReceivePersonalObject",netManScript.getServerInfo(),objectStructure.getObjectName(),
				objectStructure.getPosition(),objectStructure.getRotation(),(string)loadedPersonalRooms[lowestOpenSpot],spawnPos);
			objectStructure = fileHelper.getNextObject();
		}
	}
	
	[RPC]
	void RequestLowestOpenSpot(NetworkPlayer player)
	{
		if(Network.isServer)
		{
			networkView.RPC("ReceiveLowestOpenSpot",player,findLowestOpenRoomIndex());
		}
	}
	
	[RPC]
	void ReceiveLowestOpenSpot(int spot)
	{
		if(Network.isClient)
		{
			createPersonalRoom(Network.player, spot);
		}
	}
}
