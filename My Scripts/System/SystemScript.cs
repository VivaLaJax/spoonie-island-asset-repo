using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SystemScript : MonoBehaviour 
{
	const string SAVE_FILE_PATH = "c:\\SpoonieSaveFile.txt";
	
	public enum StoredVariables
	{
		VAR_NAME,
		VAR_POS,
		VAR_LOGGED_OUT_PLACE
	}
	
	static string[] m_strArrVariables = new string[System.Enum.GetValues(typeof(StoredVariables)).Length];
	
	// Use this for initialization
	void Awake () 
	{
		DontDestroyOnLoad(this);
	}
	
	void Start()
	{
		for(int i=0; i<m_strArrVariables.Length; i++)
		{
			m_strArrVariables[i] = "";
		}
		
		loadAllFromFile();
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public static string getNameFromArray()
	{
		string retVal = "";
		string inter = "";
		inter = getVariableByIndex((int) StoredVariables.VAR_NAME);
		bool hitColon=false;
		
		if(inter!="" && inter!=null)
		{
			for(int i=0; i<inter.Length; i++)
			{
				if(!hitColon)
				{
					if(inter[i]==':')
					{
						hitColon = true;
					}
				}
				else
				{
					retVal+=inter[i];
				}
			}
		}
		return retVal;
	}
	
	public static bool getPositionFromArray(ref Vector3 pos)
	{
		Vector3 retVal = new Vector3(0,0,0);
		string inter = getVariableByIndex((int) StoredVariables.VAR_POS);
		bool hitColon=false;
		float[] newVector = new float[3];
		int currIndex  = 0;
		string currNum = "";
		
		if(inter!="" && inter!=null)
		{
			for(int i=0; i<inter.Length; i++)
			{
				if(!hitColon)
				{
					if(inter[i]==':')
					{
						hitColon = true;
					}
				}
				else
				{
					if(inter[i]==',')
					{
						newVector[currIndex] = float.Parse(currNum);
						currIndex++;
						currNum="";
					}
					else
					{
						currNum+=inter[i];
					}
				}
			}
		}
		else
		{
			return false;
		}
		
		newVector[2] = float.Parse(currNum);
		
		retVal.x = newVector[0];
		retVal.y = newVector[1];
		retVal.z = newVector[2];
		pos = retVal;
		
		return true;
	}
	
	public static string getLoggedOutPlaceFromArray()
	{
		string retVal = "";
		string inter = "";
		inter = getVariableByIndex((int) StoredVariables.VAR_LOGGED_OUT_PLACE);
		bool hitColon=false;
		
		if(inter!="" && inter!=null)
		{
			for(int i=0; i<inter.Length; i++)
			{
				if(!hitColon)
				{
					if(inter[i]==':')
					{
						hitColon = true;
					}
				}
				else
				{
					retVal+=inter[i];
				}
			}
		}
		return retVal;
	}
	
	public static bool loadAllFromFile()
	{
		if(File.Exists(SAVE_FILE_PATH))
		{
			m_strArrVariables = File.ReadAllLines(SAVE_FILE_PATH);
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	public static string getVariableByIndex(int varInd)
	{
		return m_strArrVariables[varInd];
	}
	
	public static bool saveAllDataToFile()
	{
		string lines = "";
		
		StreamWriter file = new StreamWriter(SAVE_FILE_PATH);
		
		for(int i=0; i<m_strArrVariables.Length; i++)
		{
			if(i==m_strArrVariables.Length-1)
			{
				lines+=m_strArrVariables[i];
			}
			else
			{
				lines+=m_strArrVariables[i] + "\n";
			}
		}
		
		try
		{
			file.WriteLine(lines);
		}
		catch(IOException e)
		{
			Debug.Log(e.ToString());
			file.Close();
			return false;
		}
		
		file.Close();
		return true;
	}
	
	public static void saveVarToArray(string toSave, int index)
	{
		switch(index)
		{
		case (int) StoredVariables.VAR_NAME:
			{
				toSave= "Name:" + toSave;
			}break;
				
		case (int) StoredVariables.VAR_POS:
			{
				toSave= "Position:" + toSave;
			}break;
		case (int) StoredVariables.VAR_LOGGED_OUT_PLACE:
			{
				toSave= "LoggedOutPlace:" + toSave;
			}break;
		}
		
		m_strArrVariables[index] = toSave;
	}
}
