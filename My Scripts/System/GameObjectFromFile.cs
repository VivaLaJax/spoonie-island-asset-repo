using UnityEngine;
using System.Collections;

public class GameObjectFromFile
{
	string m_strObjectName;
	Vector3 m_vecPosition;
	Quaternion m_quatRotation;
	
	public void setObjectName(string strObjectName)
	{
		m_strObjectName = strObjectName;
	}
	
	public string getObjectName()
	{
		return m_strObjectName;
	}
	
	public void setPosition(string strPos)
	{
		string[] strArrPosArray = strPos.Split(',');
		if(strArrPosArray.Length!=3)
		{
			m_vecPosition = new Vector3(0,0,0);
		}
		else
		{
			m_vecPosition = new Vector3(float.Parse(strArrPosArray[0]), float.Parse(strArrPosArray[1]),
				float.Parse(strArrPosArray[2]));
		}
	}
	
	public Vector3 getPosition()
	{
		return m_vecPosition;
	}
	
	public void setRotation(string strRot)
	{
		string[] strArrRotArray = strRot.Split(',');
		if(strArrRotArray.Length!=4)
		{
			m_quatRotation = Quaternion.identity;
		}
		else
		{
			m_quatRotation = new Quaternion(float.Parse(strArrRotArray[0]), float.Parse(strArrRotArray[1]),
				float.Parse(strArrRotArray[2]), float.Parse(strArrRotArray[3]));
		}
	}
	
	public Quaternion getRotation()
	{
		return m_quatRotation;
	}
}