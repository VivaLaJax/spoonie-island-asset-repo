﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum TagTypes
{
	TAG_UNKNOWN,
	TAG_START,
	TAG_END,
	TAG_CHOICE,
	TAG_LINK
};

public class NPCFileReader : MonoBehaviour 
{
	const int LINK_TAG_LENGTH = 6;
	const int CHOICE_TAG_LENGTH = 8;
	
	string SAVE_FILE_DIR = "C:\\";
	string SAVE_FILE_EXTENSION = "_SpoonieNPCFile.txt";
	NonPlayerCharacterConversationScript m_NpcChat;
	public bool m_bLoggingEnabled = false;
	
	void Start () 
	{
		m_NpcChat = GetComponent<NonPlayerCharacterConversationScript>();
		//try to find file based on name
		if(loadAllFromFile())
		{
			Debug.Log("NPC File "+GetComponent<NonPlayerCharacterMain>().GetName()+ " successfully loaded");
		}
		else
		{
			Debug.Log("NPC File "+GetComponent<NonPlayerCharacterMain>().GetName()+ " was not found");
		}
	}
	
	void Update () {
	
	}
	
	public bool loadAllFromFile()
	{
		string saveFile = SAVE_FILE_DIR + GetComponent<NonPlayerCharacterMain>().GetName() + SAVE_FILE_EXTENSION;
		if(File.Exists(saveFile))
		{
			//load all the lines from the file
			List<IConversation> convoArrVariables = new List<IConversation>();
			string[] strArrFileContents = File.ReadAllLines(saveFile);
			List<string> strListConvo = new List<string>();
			for(int i=0; i<strArrFileContents.Length; ++i)
			{
				if(strArrFileContents[i].Contains("<START"))
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("Found Start line, creating new list of lines");
					}
					strListConvo.Clear();
					strListConvo.Add(strArrFileContents[i]);
				}
				else if(strArrFileContents[i].Contains("<END"))
				{
					if(m_bLoggingEnabled)
					{
						Debug.Log("Found End line, parsing block");
					}
					strListConvo.Add(strArrFileContents[i]);
					convoArrVariables.Add(ParseBlock(strListConvo));
				}
				else
				{
					strListConvo.Add(strArrFileContents[i]);
				}
			}
			
			//send them to the conversation script
			if(m_bLoggingEnabled)
			{
				Debug.Log("File Parsed. Loading conversation to NPCChat Script");
			}
			m_NpcChat.loadConversation(convoArrVariables);
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	IConversation ParseBlock(List<string> strListBlock)
	{
		IConversation convo = new IConversation();
		
		for(int i=0; i<strListBlock.Count; ++i)
		{
			if(strListBlock[i].StartsWith("<"))
			{
				int nTag = ParseTag(strListBlock[i]);
				
				switch(nTag)
				{
				//if this is a start tag
				case (int)TagTypes.TAG_START:
					{
						if(m_bLoggingEnabled)
						{
							Debug.Log("Found Start tag");
						}
						ParseStartTag(ref convo, strListBlock[i]);
						break;
					}
				case (int)TagTypes.TAG_END:
					{
						if(m_bLoggingEnabled)
						{
							Debug.Log("Found End tag, returning convo object");
						}
					
						if(convo.getLinks().Count==0)
						{
							if(m_bLoggingEnabled)
							{
								Debug.Log("No link tag parsed. Adding empty link");
							}
							convo.addLink("");
						}
						return convo;
					}
				//it's a choice
				case (int)TagTypes.TAG_CHOICE:
					{
						if(m_bLoggingEnabled)
						{
							Debug.Log("Found Choice tag, parsing line");
						}
						ParseChoiceTag(ref convo, strListBlock[i]);
						break;
					}
				case (int)TagTypes.TAG_LINK:
					{
						if(m_bLoggingEnabled)
						{
							Debug.Log("Found Link tag, parsing line");
						}

						ParseLinkTag(ref convo,strListBlock[i]);
						break;
					}
				case (int)TagTypes.TAG_UNKNOWN:
					{
						Debug.LogWarning("UNKNOWN TAG in NPC File");
						break;
					}
				}
			}
			else
			{
				if(m_bLoggingEnabled)
				{
					Debug.Log("Found Normal line, adding to convo");
				}
				convo.addDialog(strListBlock[i]);
			}
		}
		
		return convo;
	}
	
	int ParseTag(string strTagLine)
	{
		string strTag = "";
		int nTagEnd = 1;
		//get the tag word
		for(int i=1; i<strTagLine.Length; ++i)
		{
			if(strTagLine[i]==' ' || strTagLine[i]=='>')
			{
				nTagEnd = i;
				break;
			}
		}
		
		strTag = strTagLine.Substring(1,nTagEnd-1);
		
		//return the enum based on the word
		if(strTag=="START")
		{
			return (int)TagTypes.TAG_START;
		}
		else if(strTag=="END")
		{
			return (int)TagTypes.TAG_END;
		}
		else if(strTag=="LINK")
		{
			return (int)TagTypes.TAG_LINK;
		}
		else if(strTag=="CHOICE")
		{
			return (int)TagTypes.TAG_CHOICE;
		}
		
		return (int)TagTypes.TAG_UNKNOWN;
	}
	
	void ParseStartTag(ref IConversation convo, string strBlockLine)
	{
		string strConvoType = "";
		int nCurrIndex = 7;
		
		//work out what type of convo it is
		for(int i=7; i<strBlockLine.Length; ++i)
		{
			if(strBlockLine[i]==':')
			{
				nCurrIndex = i;
				break;
			}
			else
			{
				strConvoType+=strBlockLine[i];
			}
		}
		
		convo = new IConversation();
		if(strConvoType=="conversation")
		{
			convo.setType((int)ConversationTypeDefs.CONVO_TYPE_DIALOGUE);
		}
		else if(strConvoType=="choice_convo")
		{
			convo.setType((int)ConversationTypeDefs.CONVO_TYPE_CHOICE);
		}
		else
		{
			Debug.LogError("Incorrect conversation type in start tag.");
			return;
		}
		
		string strTitle = "";
		bool bStringFound = false;
		//assign the title	
		for(int i=nCurrIndex; i<strBlockLine.Length; ++i)
		{
			if(strBlockLine[i].Equals('\"') && !bStringFound)
			{
				bStringFound = true;
			}
			else if(strBlockLine[i].Equals('\"') && bStringFound)
			{
				if(m_bLoggingEnabled)
				{
					Debug.Log("Found title in Start tag: " + strTitle);
				}
				convo.setTitle(strTitle);
				return;
			}
			else if(bStringFound)
			{
				strTitle+=strBlockLine[i];
			}
		}
		
		if(m_bLoggingEnabled)
		{
			Debug.Log("Could not find full title in tagged line: " + strTitle);
		}
	}
	
	void ParseChoiceTag(ref IConversation convo, string strBlockLine)
	{
		//if our convo is the right type
		if(convo.getType()!=(int)ConversationTypeDefs.CONVO_TYPE_CHOICE)
		{
			return;
		}
		//take out the text and add it to the
		string strText = strBlockLine.Substring(CHOICE_TAG_LENGTH);
		//choices array
		if(m_bLoggingEnabled)
		{
			Debug.Log("Found Choice: " + strText);
		}
		convo.addChoice(strText);
	}
	
	void ParseLinkTag(ref IConversation convo, string strBlockLine)
	{
		//add to the links
		string strText = strBlockLine.Substring(LINK_TAG_LENGTH);
		if(m_bLoggingEnabled)
		{
			Debug.Log("Found link: " + strText);
		}
		convo.addLink(strText);
	}
}
