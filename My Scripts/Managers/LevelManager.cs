using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour 
{
	ArrayList CharsInLevel;
	public string levelName;
	
	ArrayList networkedObjs;
	ArrayList createdObjs;
	NetworkManagerScript netManScript;
	
	public Material m_matSkyBox;
	public GameObject netSlotOne;
	
	void Awake()
	{
		GameObject netMan = GameObject.FindGameObjectWithTag("NetworkManager");
		netManScript = netMan.GetComponent<NetworkManagerScript>();
		
		if(Network.isServer)
		{
			networkView.viewID = netManScript.GetNextId();
		}
	}

	void Start()
	{
		networkedObjs = new ArrayList();
		createdObjs = new ArrayList();
		CharsInLevel = new ArrayList();
		netManScript.levelManagerLoaded();
		
		if(netSlotOne!=null)
			networkedObjs.Add(netSlotOne);
	}
	
	public void addChar(NetworkPlayer character)
	{
		Debug.Log("Character added to Level: " + character);
		CharsInLevel.Add(character);
		Debug.Log("Num Characters in Level: " + CharsInLevel.Count);
		
		if(Network.isServer)
		{
			int charOfID = -99;
			GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");
			for(int j=0; j<playerChars.Length; j++)
			{
				PlayerCharacterScript charScript = playerChars[j].GetComponent<PlayerCharacterScript>();
				if(charScript.GetOwner()==character)
				{
					charOfID = j;
				}
			}
			
			if(charOfID!=-99)
			{
				NetworkViewID id = playerChars[charOfID].GetComponent<NetworkView>().viewID;
	
				for(int i=0; i<CharsInLevel.Count; i++)
				{
					networkView.RPC("CreateNetPlayer", (NetworkPlayer)CharsInLevel[i], id, character);
				}
			}
		}
	}
	
	public NetworkPlayer getChar(int index)
	{
		return (NetworkPlayer) CharsInLevel[index];
	}
	
	public NetworkViewID getViewId()
	{
		return networkView.viewID;
	}
	
	public bool isCharacterInLevel(NetworkPlayer player)
	{
		return CharsInLevel.Contains(player);
	}
	
	//remove character from list
	public void removeChar(NetworkPlayer character)
	{
		CharsInLevel.Remove(character);
		Debug.Log (getLevelName()+" Count: " + CharsInLevel.Count);
		checkForEmpty();
		
		if(Network.isServer)
		{			
			for(int i=0; i<CharsInLevel.Count; i++)
			{
				networkView.RPC("playerHasLeftLevel", (NetworkPlayer)CharsInLevel[i], character);
			}
		}
	}
	
	public string getLevelName()
	{
		return levelName;
	}
	
	public void checkForEmpty()
	{
		//if there are no characters left
		if(CharsInLevel.Count==0)
		{
			//delete all things related to the level
			netManScript.deleteLevelAssets(levelName);
		}
	}
	
	void Update()
	{
		
	}

	public void Setup()
	{
		Vector3 pos = transform.position;
      
		for(int i=0; i<networkedObjs.Count; i++)
		{
			GameObject netObj = (GameObject) Instantiate((GameObject)networkedObjs[i], pos, Quaternion.identity);
			netObj.GetComponent<NetworkView>().viewID = netManScript.GetNextId();
			createdObjs.Add(netObj);
		}
	}
	
	public void createNetObjs(NetworkPlayer player)
	{
		for(int i=0; i<networkedObjs.Count; i++)
		{
			NetworkViewID id = ((GameObject)createdObjs[i]).GetComponent<NetworkView>().viewID;
			Debug.Log("Sending RPC with index: " + i + " and id: " + id);
			networkView.RPC("CreateNetObjByIndex", player, i, id);
		}
      
		GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");
		
		if(levelName!="MasterPersonalRoom")
		{
			for(int i=0; i<CharsInLevel.Count; i++)
			{	
				for(int j=0; j<playerChars.Length; j++)
				{
					PlayerCharacterScript charScript = playerChars[j].GetComponent<PlayerCharacterScript>();
					if(charScript.GetOwner()==((NetworkPlayer)CharsInLevel[i])&&charScript.GetOwner()!=player)
					{
						NetworkViewID id = charScript.GetComponent<NetworkView>().viewID;
						
						networkView.RPC("CreateNetPlayer", player, id, charScript.GetOwner());
						break;
					}
				}
			}
		}
	}
   
	[RPC]
	void CreateNetObjByIndex(int index, NetworkViewID newId)
	{
		Vector3 pos = transform.position;
		Debug.Log("Creating object: " + index);
		GameObject netObj = (GameObject) Instantiate((GameObject)networkedObjs[index], pos, Quaternion.identity);
		Debug.Log("Assigning view id: " + newId);
		netObj.GetComponent<NetworkView>().viewID = newId;
		createdObjs.Add(netObj);
	}

	[RPC]
	void CreateNetPlayer(NetworkViewID viewId, NetworkPlayer owner)
	{
		if(owner!=Network.player)
		{
			Debug.Log("Networked player being created. Level: " + levelName);
			netManScript.SpawnPlayer(owner, viewId, levelName);
		}
	}

	[RPC]
	void playerHasLeftLevel(NetworkPlayer player)
	{
		if(player!=Network.player)
		{
			netManScript.DestroyPlayer(player);
		}
	}
	
	public void recieveId(NetworkViewID myId)
	{
		networkView.viewID = myId;
	}
	
	public void UpdateSkyboxes()
	{
		GameObject[] goCameras = GameObject.FindGameObjectsWithTag("MainCamera");
		foreach(GameObject goCamera in goCameras)
		{
			if(goCamera.GetComponent<Skybox>()!=null)
			{
				goCamera.GetComponent<Skybox>().material = m_matSkyBox;
			}
		}
	}
}
