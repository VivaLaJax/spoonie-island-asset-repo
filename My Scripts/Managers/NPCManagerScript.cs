﻿using UnityEngine;
using System.Collections;

public class NPCManagerScript : MonoBehaviour 
{
	void Start () 
	{
		
	}
	
	void Update () 
	{
		
	}
	
	public void ActionButtonPressed(NetworkPlayer player)
	{
		GameObject[] goNPCs = GameObject.FindGameObjectsWithTag("NPC");
		
		for(int i=0; i<goNPCs.Length; i++)
		{
			if(goNPCs[i].activeInHierarchy)
			{
				goNPCs[i].GetComponent<NonPlayerCharacterMain>().On_ActionButton_Pressed(player);
			}
		}
	}
}
