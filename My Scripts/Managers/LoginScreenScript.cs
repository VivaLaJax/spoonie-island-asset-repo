using UnityEngine;
using System.Collections;

public class LoginScreenScript : MonoBehaviour 
{
	public GameObject netManPrefab;
	public GameObject systemObjPrefab;
	GameObject netMan;
	GameObject systemObj;
	int state; 
	private float btnX;
	private float btnY;
	private float btnW;
	private float btnH;
	string toSend;
	
	public enum LoginState
	{
		STATE_CHECK_NAME,
		STATE_NAME_INPUT,
		STATE_NAME_RECEIVED
	}
	
	// Use this for initialization
	void Start () 
	{
		state = (int) LoginState.STATE_CHECK_NAME;
		
		netMan = GameObject.FindGameObjectWithTag("NetworkManager");
		
		if(netMan!=null)
		{
			Destroy(netMan);
		}
		
		Instantiate(netManPrefab);
		
		systemObj = GameObject.FindGameObjectWithTag("SystemObject");
		
		if(systemObj!=null)
		{
			Destroy(systemObj);
		}
		
		Instantiate(systemObjPrefab);
		
		btnX = (float)(Screen.width * 0.05);
		btnY = (float)(Screen.width * 0.05);
		btnW = (float)(Screen.width * 0.1);
		btnH = (float)(Screen.width * 0.1);
		toSend = "";
	}
	
	void Update()
	{
		switch(state)
		{
			//check if you can get a name from file
			//if so move into netmanager
			case (int) LoginState.STATE_CHECK_NAME:
			{	
				if(SystemScript.getNameFromArray()!="")
				{
					state = (int) LoginState.STATE_NAME_RECEIVED;
				}
				else
				{
					state = (int) LoginState.STATE_NAME_INPUT;
				}
			
				break;
			}
			//get the name from input and create a file
			case (int) LoginState.STATE_NAME_INPUT:
			{
				//show a text box for inputting text, with string in it
				foreach (char c in Input.inputString) 
				{
           			if (c == "\b"[0])
					{
               			if (toSend.Length != 0)
						{
                   			toSend = toSend.Substring(0, toSend.Length - 1);
						}
					}
					else
					{
						toSend+=c;
					}
				}
			
				break;
			}
			//nothing let netman take over
			case (int) LoginState.STATE_NAME_RECEIVED:
			{
				//nada right now
				break;
			}
		}
	}
	
	void OnGUI()
	{
		if(state==(int) LoginState.STATE_NAME_INPUT)
		{
			//show box with text being typed here
			GUI.Box(new Rect(btnX,btnY,btnW,btnH), toSend);
			
			if(GUI.Button(new Rect(btnX,(float)(btnY*1.2+btnH),btnW,btnH), "Enter Name"))
			{
				//create file and save name to it
				SystemScript.saveVarToArray(toSend, (int) SystemScript.StoredVariables.VAR_NAME);
				//change to state received
				state = (int) LoginState.STATE_NAME_RECEIVED;
			}
		}
	}
	
	public int getCurrentState()
	{
		return state;
	}
}
