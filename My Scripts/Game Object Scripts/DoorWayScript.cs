using UnityEngine;
using System.Collections;

public class DoorWayScript : MonoBehaviour 
{
	public string source;
	public string destination;
	public Vector3 bufferVector;
	public bool hasBeenActivated;
	
	// Use this for initialization
	void Start () 
	{
		hasBeenActivated = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(!Network.isServer)
		{
			if(!hasBeenActivated)
			{
				if(other.tag=="Player")
				{
					PlayerCharacterScript playScript = other.gameObject.GetComponent<PlayerCharacterScript>();
					if(playScript.GetOwner()==Network.player)
					{
						GameObject netMan = GameObject.FindGameObjectWithTag("NetworkManager");
						NetworkManagerScript netScript = netMan.GetComponent<NetworkManagerScript>();
						
						netScript.RequestLevelFromPortal(destination,source,Network.player);
					
						hasBeenActivated = true;
					}
				}
			}
		}
	}
	
	void OnDrawGizmos()
	{
		Gizmos.DrawCube(this.gameObject.transform.position, ((BoxCollider)this.gameObject.collider).size);
	}
	
	public string getSource()
	{
		return source;
	}
	
	public string getDestination()
	{
		return destination;
	}
	
	public Vector3 getBufferVector()
	{
		return bufferVector;
	}
	
}
