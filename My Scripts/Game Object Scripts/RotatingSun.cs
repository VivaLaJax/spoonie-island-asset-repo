using UnityEngine;
using System.Collections;

public class RotatingSun : MonoBehaviour 
{
	private Quaternion lastRotation;
	private NetworkManagerScript netManScript;
	
	public float m_nDayCycleInMinutes = 1;
	
	private float m_nDegreeRotation;
	private float m_nTimeOfDay;
	
	private const float SECOND = 1f;
	private const float MINUTE_IN_SEC = 60*SECOND;
	private const float HOUR_IN_SEC = 60*MINUTE_IN_SEC;
	private const float DAY_IN_SEC = 24*HOUR_IN_SEC;
	private const float YEAR_IN_SEC = 365*DAY_IN_SEC;
	private const float DEGREES_PER_SECOND = 360/DAY_IN_SEC;
	
	void Start () 
	{
		lastRotation = Quaternion.identity;
		netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		m_nTimeOfDay = 0;
		m_nDegreeRotation = DEGREES_PER_SECOND*DAY_IN_SEC/ (m_nDayCycleInMinutes*MINUTE_IN_SEC);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Network.isServer)
		{
			m_nTimeOfDay += Time.deltaTime;
			
			gameObject.transform.Rotate(new Vector3(m_nDegreeRotation,0,0)*Time.deltaTime);
			
			if(transform.rotation.x!=lastRotation.x)
			{
				Debug.Log("Rotation Change");
				LevelManager levelMan = netManScript.getLevelManager(gameObject.GetComponent<LevelAssetScript>().getLevelName());
				for(int i=0; i<Network.connections.Length; i++)
				{
					if(levelMan.isCharacterInLevel(Network.connections[i]))
					{
						networkView.RPC ("updatePosition",Network.connections[i],transform.position, transform.rotation, m_nTimeOfDay);
					}
				}
			}
			
			lastRotation.x = transform.rotation.x;
		}
	}
	
	[RPC]
	void updatePosition(Vector3 updatedPosition, Quaternion updatedRotation, float tod)
	{
		if(!Network.isServer)
		{
			m_nTimeOfDay = tod;
			transform.position = updatedPosition;
			transform.rotation = updatedRotation;
		}
	}
}
