using UnityEngine;
using System.Collections;

public class MobileBlockScript : MonoBehaviour 
{
	private Vector3 startPos;
	private Vector3 endPos;
	private Transform myTransform;
	
	private float i;
	
	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
		i = 0;
		startPos = myTransform.position;
		endPos = new Vector3(myTransform.position.x+10, myTransform.position.y, myTransform.position.z+10);
		myTransform.LookAt(endPos);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(i>=1)
		{
			endPos = startPos;
			startPos = myTransform.position;
			myTransform.LookAt(endPos);
			i=0;
		}
		else
		{
			i+=0.01f;
			transform.position = Vector3.Lerp(startPos, endPos, i);
		}
	}
}


