﻿using UnityEngine;
using System.Collections;

public class PlayerAnimationController : MonoBehaviour 
{
	public AnimationClip m_animWalk;
	public AnimationClip m_animWave;
	public AnimationClip m_animIdle;
	
	private Animation m_Animation;
	
	private bool m_bAnimComplete;
	private bool m_bTimerStarted;
	
	public int m_nCurrentState;
	public enum CharacterState
	{
		STATE_IDLE,
		STATE_WALKING,
		STATE_WAVING
	};
	
	
	void Start () 
	{
		m_nCurrentState = (int)CharacterState.STATE_IDLE;
		m_bTimerStarted = false;
	}
	
	void Update () 
	{
		switch(m_nCurrentState)
		{
		case (int) CharacterState.STATE_IDLE:
			{
				m_Animation = GetComponentInChildren<Animation>();
				m_Animation[m_animIdle.name].speed = 0.5f;
				m_Animation.CrossFade(m_animIdle.name);
				break;
			}
		case (int) CharacterState.STATE_WALKING:
			{
				m_Animation = GetComponentInChildren<Animation>();
				m_Animation[m_animWalk.name].speed = 1.5f;
				m_Animation.CrossFade(m_animWalk.name);	
				break;
			}
		case (int) CharacterState.STATE_WAVING:
			{
				m_Animation = GetComponentInChildren<Animation>();
				m_Animation[m_animWave.name].speed = 1f;
				m_Animation.Play(m_animWave.name);
				
				if(!m_bTimerStarted)
				{
					m_bTimerStarted = true;
					StartCoroutine(AnimationTimer(m_animWave.length));
				}
				
				if(m_bAnimComplete)
				{
					SetState((int)CharacterState.STATE_IDLE);	
				}
				break;
			}
		default:
			{
				Debug.LogWarning("No animation state set! Ahh!!");
				break;
			}
		}
		
	}
	
	public void SetState(int nState)
	{
		m_nCurrentState = nState;
		m_bTimerStarted = false;
		m_bAnimComplete = false;
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager")
			.GetComponent<NetworkManagerScript>();
		networkView.RPC ("SetNetworkState", netManScript.getServerInfo(),nState,Network.player);
	}
	
	[RPC]
	public void SetNetworkState(int nState, NetworkPlayer player)
	{
		if(Network.isServer)
		{
			for(int i=0; i<Network.connections.Length; i++)
			{
				PlayerCharacterScript playerScript = gameObject.GetComponent<PlayerCharacterScript>();
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				if(Network.connections[i]!=player && netManScript.ArePlayersInSameLevel(Network.connections[i],netManScript.GetLevelNameFromIndex(playerScript.GetCurrentLevel())))
				{
					networkView.RPC("SetNetworkState",Network.connections[i],nState, Network.player);
				}
			}
		}
		else
		{
			m_nCurrentState = nState;
			m_bTimerStarted = false;
			m_bAnimComplete = false;
		}
	}
	
	IEnumerator AnimationTimer(float waitTime)
	{
		m_bAnimComplete = false;
		yield return new WaitForSeconds(waitTime);
    	m_bAnimComplete = true;
    }
}
