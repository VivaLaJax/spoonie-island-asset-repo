﻿using UnityEngine;
using System.Collections;

public class DirectionChanger : MonoBehaviour 
{
	public void changeDirection(Vector3 vecMove)
	{
		//face left
		if(vecMove.x<0)
		{
			transform.forward = new Vector3(-1f, 0f, 0f);
		}
		//face right
		else if(vecMove.x>0)
		{
			transform.forward = new Vector3(1f, 0f, 0f);
		}
		//face back
		else if(vecMove.z<0)
		{
			transform.forward = new Vector3(0f, 0f, -1f);
		}
		//face forward
		else if(vecMove.z>0)
		{
			transform.forward = new Vector3(0f, 0f, 1f);
		}
		else
		{
			transform.forward = new Vector3(0f, 0f, 1f);
		}
	}
}
