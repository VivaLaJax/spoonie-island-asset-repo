using UnityEngine;
using System.Collections;

public class BillboardScript : MonoBehaviour 
{
	public Camera m_Camera = null;
 
    void Update()
    {
		if(m_Camera!=null)
		{
        	transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.back,
          	  m_Camera.transform.rotation * Vector3.up);
		}
    }
	
	public void setCam(Camera cam)
	{
		m_Camera = cam;
	}
}
