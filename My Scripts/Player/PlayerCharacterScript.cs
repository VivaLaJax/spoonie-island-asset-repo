using UnityEngine;
using System.Collections;

public class PlayerCharacterScript : MonoBehaviour 
{
	NetworkPlayer myOwner;
	string m_CharName;
	private int currentState;
	public int currLevel;
	int sessionId;
	public GameObject blockPref;
	public GameObject movingBlockPref;
	NetworkManagerScript netManScript;
	
	public float m_nCharSpeed = 2.0f;
	
	enum actionButtonState
	{
		CREATE_STATIC_OBJECT,
		CREATE_MOBILE_OBJECT,
		STATE_WAVE
	}
		
	void Start () 
	{
		sessionId = -1;
		netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		m_CharName = SystemScript.getNameFromArray();
		currentState = 0;
		/*currLevel = netManScript.getLevelIndex(SystemScript.getLoggedOutPlaceFromArray());
		if(currLevel==-99)
		{
			currLevel=netManScript.getLevelIndex("CentralSquare");
		}*/
	}
	void Update () 
	{
		if(Network.player==GetOwner())
		{			
			if(Input.GetKeyDown(KeyCode.Alpha1))
			{
				Debug.Log("Player Character State: Create Static");
				currentState = (int) actionButtonState.CREATE_STATIC_OBJECT;
				Vector3 addOn = new Vector3(0,0,-transform.localScale.z);
				networkView.RPC("requestStaticObj",netManScript.getServerInfo(),transform.position+addOn, Network.player);
			}
			else if(Input.GetKeyDown(KeyCode.Alpha2))
			{
				Debug.Log("Player Character State: Create Mobile");
				currentState = (int) actionButtonState.CREATE_MOBILE_OBJECT;
				Vector3 addOn = new Vector3(0,0,transform.localScale.z);
				networkView.RPC("requestMobileObj",netManScript.getServerInfo(),transform.position+addOn);
			}
			else if(Input.GetKeyDown(KeyCode.Alpha3))
			{
				Debug.Log("Player Character State: Wave");
				currentState = (int) actionButtonState.STATE_WAVE;
				Wave();
			}
		}
	}
	
	
	
	////////////////////////////////////////////////////////////////////
	public void SetOwner(NetworkPlayer newOwner)
	{
		myOwner = newOwner;
		Debug.Log ("SetOwner: myOwner= " +myOwner.ToString());
		GetComponentInChildren<PlayerCamScript>().enableCamera(myOwner);
	}
	
	void setId(int id)
	{
		sessionId = id;
	}
	
	int getId()
	{
		return sessionId;
	}
	
	public NetworkPlayer GetOwner()
	{
		return myOwner;
	}
	
	public string getName()
	{
		return m_CharName;
	}
	
	public void SetCurrentLevel(int level)
	{
		currLevel = level;
	}
	
	public int GetCurrentLevel()
	{
		return currLevel;
	}
	
	[RPC]
	void requestStaticObj(Vector3 pos, NetworkPlayer player)
	{
		createStaticObj(pos, player);
		//ought to send to only chars in level
		for(int i=0; i<Network.connections.Length; i++)
		{
			networkView.RPC("createStaticObj", Network.connections[i], pos, player);
		}
	}

	[RPC]
	void requestMobileObj(Vector3 pos)
	{
		NetworkViewID nextId = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>().GetNextId();
		
		createMobileObj(pos,nextId);
		for(int i=0; i<Network.connections.Length; i++)
		{
			networkView.RPC("createMobileObj", Network.connections[i], pos, nextId);
		}
	}

	[RPC]
	void createStaticObj(Vector3 pos, NetworkPlayer player)
	{
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		GameObject block = (GameObject) Instantiate(blockPref, pos, Quaternion.identity);
		string levelName = "";
		if(currLevel<0)
		{
			levelName = PersonalRoomManagerScript.LEVEL_MASTER_NAME + player;
		}
		else
		{
			levelName = netManScript.GetLevelNameFromIndex(currLevel);
		}
		block.GetComponent<LevelAssetScript>().setLevelName(levelName);
	}

	[RPC]
	void createMobileObj(Vector3 pos, NetworkViewID nextId)
	{
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		GameObject block = (GameObject) Instantiate(movingBlockPref, pos, Quaternion.identity);
		block.GetComponent<LevelAssetScript>().setLevelName(netManScript.GetLevelNameFromIndex(currLevel));
		
		NetworkView netView = block.GetComponent<NetworkView>();
		netView.viewID = nextId;
	}
	
	////////////////////////////////////////////////////////////////////
	void Wave()
	{
		GetComponent<PlayerAnimationController>().SetState((int)PlayerAnimationController.CharacterState.STATE_WAVING);
	}
	
	////////////////////////////////////////////////////////////////////
	public float GetSpeed()
	{
		return m_nCharSpeed;
	}
}
