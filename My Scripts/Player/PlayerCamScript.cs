using UnityEngine;
using System.Collections;

public class PlayerCamScript : MonoBehaviour 
{
	void Awake ()
	{
    	camera.enabled = false;
		GetComponent<AudioListener>().enabled = false;
	}
	
	public void enableCamera(NetworkPlayer owner)
	{
		if(Network.player==owner)
		{
    		camera.enabled = true;
			GetComponent<AudioListener>().enabled = true;
    	}
	}
}
