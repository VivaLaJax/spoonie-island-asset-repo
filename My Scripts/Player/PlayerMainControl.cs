using UnityEngine;
using System.Collections;

public class PlayerMainControl : MonoBehaviour 
{
	bool chatting = false;
	bool menuOn = false;
	string[] buttonNames = {"Logout"};
	//public Texture2D mainMenuBack;
	//public Texture2D buttonBack;
	public GUIStyle menuStyle;
	public GUIStyle buttonStyle;
	
	float padding = 10f;
	float menuWidth = 150f;
	float buttonHeight = 28f;
	
	
	void Start()
	{
		SystemScript.loadAllFromFile();
	}
	
	void Update() 
	{
		if(!chatting)
		{
			if(!menuOn)
			{
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					menuOn = true;
				}
				else if(Input.GetButtonDown("PlayerActionButton"))
				{
					NetworkPlayer player = GetComponent<PlayerCharacterScript>().GetOwner();
					if(player==Network.player)
					{
						GameObject goNPCMan = GameObject.FindGameObjectWithTag("NPCManager");
						
						if(goNPCMan)
						{
							NPCManagerScript npcScript = goNPCMan.GetComponent<NPCManagerScript>();
							npcScript.ActionButtonPressed(player);
						}
					}
				}
			}
			else
			{
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					menuOn = false;
				}
			}
		}
	}
	
	void OnGUI()
	{
		if(menuOn)
		{
			GUI.Box(new Rect(0,0,menuWidth,(buttonNames.Length+1)*buttonHeight),"Main Menu",menuStyle);
			for(int i=0; i<buttonNames.Length; i++)
			{
				if(GUI.Button(new Rect(0+padding,(i+1)*buttonHeight,menuWidth-(2*padding),buttonHeight),buttonNames[i],buttonStyle))
				{
					if(buttonNames[i]=="Logout")
					{
						Logout();
					}
				}
			}
		}
	}
	
	void OnChatting()
	{
		chatting = !chatting;
	}
	
	void Logout()
	{
		Debug.Log("Logging Out");
		PlayerCharacterScript pcScript = gameObject.GetComponent<PlayerCharacterScript>();
		NetworkManagerScript netMan = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();;

		//save your position
		string saveVar = "" + gameObject.transform.position.x + "," + gameObject.transform.position.y + "," + gameObject.transform.position.z;
		SystemScript.saveVarToArray(saveVar, (int) SystemScript.StoredVariables.VAR_POS);
		
		int levelIndex = pcScript.GetCurrentLevel();
		
		if(levelIndex<0)
		{
			GameObject goFloor = GameObject.FindGameObjectWithTag("Floor");
			
			//save the room assets
			PersonalRoomFileHelper fileHelper = ScriptableObject.CreateInstance<PersonalRoomFileHelper>();
			fileHelper.savePersonalLevelAssets(goFloor.GetComponent<LevelAssetScript>().getLevelName());
					
			saveVar = "MasterPersonalRoom";
		}
		else
		{
			saveVar = netMan.GetLevelNameFromIndex(levelIndex);
		}
		SystemScript.saveVarToArray(saveVar, (int) SystemScript.StoredVariables.VAR_LOGGED_OUT_PLACE);
		
		if(!SystemScript.saveAllDataToFile())
		{
			Debug.Log("Save file has failed.");
		}

		Network.Disconnect();
	}
}
