using UnityEngine;
using System.Collections;

public class ChatScript : MonoBehaviour 
{
	bool beingShown;
	string toSend;
	public float width = 200f;
	public float height = 30f;
	
	void Start () 
	{
		beingShown = false;
		toSend = "";
		DisableSpeechBubble();
	}
	
	void Update () 
	{
		if(gameObject.GetComponent<PlayerCharacterScript>().GetOwner()==Network.player)
		{
			//if enter is pressed show text box and tell other scripts
			if(!beingShown)
			{
				if(Input.GetKeyDown(KeyCode.Return))
				{
					beingShown = true;
					gameObject.SendMessage("OnChatting", SendMessageOptions.DontRequireReceiver);
				}
			}
			else
			{
				//check for enter to output the string
				if(Input.GetKeyDown(KeyCode.Return))
				{
					NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
					netManScript.distributeChatMessage(toSend,Network.player);
					
					//outputText(toSend);
					
					toSend = "";
					beingShown = false;
					gameObject.SendMessage("OnChatting", SendMessageOptions.DontRequireReceiver);
				}
				//if escape is pressed, turn the box off
				else if(Input.GetKeyDown(KeyCode.Escape))
				{
					beingShown = false;
					gameObject.SendMessage("OnChatting", SendMessageOptions.DontRequireReceiver);
				}
				else
				{
					//show a text box for inputting text, with string in it
					foreach (char c in Input.inputString) 
					{
            			if (c == "\b"[0])
						{
                			if (toSend.Length != 0)
							{
                    			toSend = toSend.Substring(0, toSend.Length - 1);
							}
						}
						else
						{
							toSend+=c;
						}
					}
				}
			}
		}
	}
	
	void OnGUI()
	{	
		if(beingShown)
		{
			//show box with text being typed here
			GUI.Box(new Rect(Screen.width/2-width/2,Screen.height-2*height,width,height), toSend);
		}
	}
	
	public void outputText(string toSend)
	{
		EnableSpeechBubbleWithText(toSend);
		StartCoroutine("StartBubbleTimer");
	}
	
	void EnableSpeechBubbleWithText(string toSend)
	{
		if(Network.isClient)
		{
			ObjectLabel[] obLabels = gameObject.GetComponentsInChildren<ObjectLabel>();
			for(int i=0; i<obLabels.Length; i++)
			{
				obLabels[i].enabled = true;
			}
			
			Transform bubble = (Transform) transform.FindChild("SpeechBubble");
			GUITexture texture = bubble.GetComponent<GUITexture>();
			texture.enabled = true;
			GUIText text = bubble.GetComponent<GUIText>();
			text.enabled = true;
		
			Transform nameTag = (Transform) transform.FindChild("SpeechName");
			GUITexture nameTexture = nameTag.GetComponent<GUITexture>();
			nameTexture.enabled = true;
			text = nameTag.GetComponent<GUIText>();
			text.enabled = true;
		
			CamText speech = bubble.GetComponent<CamText>();
			speech.text = toSend;
		
			speech = nameTag.GetComponent<CamText>();
			speech.text = gameObject.GetComponent<PlayerCharacterScript>().getName();
		}
	}
	
	void DisableSpeechBubble()
	{
		Transform bubble = (Transform) transform.FindChild("SpeechBubble");
		GUITexture texture = bubble.GetComponent<GUITexture>();
		texture.enabled = false;
		GUIText text = bubble.GetComponent<GUIText>();
		text.enabled = false;
	
		Transform nameTag = (Transform) transform.FindChild("SpeechName");
		GUITexture nameTexture = nameTag.GetComponent<GUITexture>();
		nameTexture.enabled = false;
		text = nameTag.GetComponent<GUIText>();
		text.enabled = false;
		
		CamText speech = bubble.GetComponent<CamText>();
		speech.text = "";
			
		ObjectLabel[] obLabels = gameObject.GetComponentsInChildren<ObjectLabel>();
		for(int i=0; i<obLabels.Length; i++)
		{
			obLabels[i].enabled = false;
		}
	}
	
	IEnumerator StartBubbleTimer()
	{
		float waitTime = 5.0f;
		
		yield return new WaitForSeconds(waitTime);
		
		DisableSpeechBubble();
	}
}
