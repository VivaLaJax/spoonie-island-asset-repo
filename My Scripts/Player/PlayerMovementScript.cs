using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour 
{
	public int speed = 7;
	public int gravity = 20;
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;
	private bool chatting;
	private Vector3 lastPosition;
	
	private PlayerAnimationController m_AnimationController;
	
	private float m_nLastMoveX;
	private float m_nLastMoveZ;
	
	void Awake()
	{
		//DontDestroyOnLoad(this);
		chatting = false;
	}
	
	// Use this for initialization
	void Start () {
		controller=GetComponent<CharacterController>();
		m_AnimationController = GetComponent<PlayerAnimationController>();
		m_nLastMoveX = 0f;
		m_nLastMoveZ = 0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Network.player==GetComponent<PlayerCharacterScript>().GetOwner())
		{
			if (controller.isGrounded) 
			{
				float nSpeed = GetComponent<PlayerCharacterScript>().GetSpeed();
				if(!chatting)
				{
	           		moveDirection = new Vector3(-Input.GetAxis("Horizontal")*nSpeed, 0, -Input.GetAxis("Vertical")*nSpeed);
				}
				else
				{
					moveDirection = new Vector3(0,0,0);	
				}
        	   	moveDirection *= speed;
			}
			moveDirection.y -= gravity * Time.deltaTime;
        	controller.Move(moveDirection * Time.deltaTime);
			
			if(moveDirection.x!=0f || moveDirection.z!=0f)
			{
				m_AnimationController.SetState((int)PlayerAnimationController.CharacterState.STATE_WALKING);
			}
			else
			{
				//if we were moving last time, set us idle
				if(m_nLastMoveX!=0f || m_nLastMoveZ !=0f)
				{
					m_AnimationController.SetState((int)PlayerAnimationController.CharacterState.STATE_IDLE);
				}
			}
			
			GetComponentInChildren<DirectionChanger>().changeDirection(moveDirection);
			NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager")
				.GetComponent<NetworkManagerScript>();
			networkView.RPC("SetNetCharDirection",netManScript.getServerInfo(),moveDirection, Network.player);
			
			if(transform.position!=lastPosition)
			{
				//NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				networkView.RPC ("updatePosition",netManScript.getServerInfo(),transform.position, Network.player);
			}
			
			lastPosition = transform.position;
			m_nLastMoveX = moveDirection.x;
			m_nLastMoveZ = moveDirection.z;
		}
	}
	
	public void OnChatting()
	{
		chatting = !chatting;
	}
	
	[RPC]
	void updatePosition(Vector3 updatedPosition, NetworkPlayer player)
	{
		if(Network.isServer)
		{
			for(int i=0; i<Network.connections.Length; i++)
			{
				PlayerCharacterScript playerScript = gameObject.GetComponent<PlayerCharacterScript>();
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				if(Network.connections[i]!=player && netManScript.ArePlayersInSameLevel(Network.connections[i],netManScript.GetLevelNameFromIndex(playerScript.GetCurrentLevel())))
				{
					networkView.RPC ("updatePosition",Network.connections[i],transform.position, Network.player);
				}
			}
		}
		
		transform.position = updatedPosition;
	}
	
	[RPC]
	public void SetNetCharDirection(Vector3 vec3Move, NetworkPlayer player)
	{
		if(Network.isServer)
		{
			for(int i=0; i<Network.connections.Length; i++)
			{
				PlayerCharacterScript playerScript = gameObject.GetComponent<PlayerCharacterScript>();
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				if(Network.connections[i]!=player && netManScript.ArePlayersInSameLevel(Network.connections[i],netManScript.GetLevelNameFromIndex(playerScript.GetCurrentLevel())))
				{
					networkView.RPC("SetNetCharDirection",Network.connections[i],vec3Move, Network.player);
				}
			}
		}
		else
		{
			GetComponentInChildren<DirectionChanger>().changeDirection(vec3Move);
		}
	}
}
